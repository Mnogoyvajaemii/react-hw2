import { Component } from "react";
import "./productCard.scss";
import { StarIcon, StarFillIcon } from "../icons";
import Modal from "../modal";
import PropTypes from "prop-types";

class ProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = { cardData: null, favorite: false, showModal: false };

    this.handleShowModal = this.handleShowModal.bind(this);
    this.handleHideModal = this.handleHideModal.bind(this);
  }

  handleShowModal() {
    this.setState({
      showModal: true,
    });
  }

  handleHideModal() {
    this.setState({
      showModal: false,
    });
  }

  componentDidMount() {
    this.props.addToFav(this.props.id);
  }

  // addToFav(id) {
  //   const arr = this.props.favItems;
  //   if (!arr.includes(id)) {
  //     arr.push(id);
  //   } else {
  //     const index = arr.findIndex((item) => item === id);
  //     arr.splice(index, 1);
  //   }

  //   localStorage.setItem("favItems", JSON.stringify(arr));
  //   this.setState({ favorite: arr.includes(id) });
  //   return arr;
  // }

  render() {
    const isFav = this.props.favItems.includes(this.props.id);
    const { showModal } = this.state;
    return (
      <>
        <li className="product-card">
          <div
            onClick={() => {
              this.props.addToFav(this.props.id);
              this.setState({ favorite: !this.state.favorite });
            }}
            style={{ cursor: "pointer" }}
          >
            {isFav ? <StarFillIcon /> : <StarIcon />}
          </div>
          <a className="product-card__link" href="">
            <img className="product-card__image" src={this.props.image} alt="Card paint" />
          </a>
          <a className="product-card__link card__link--name" href="">
            <h2 className="product-card__title">{this.props.name}</h2>
          </a>
          <p className="product-card__color">
            Колір - <span>{this.props.color}</span>
          </p>
          <p className="product-card__price">
            Ціна : <span>{this.props.price}</span>
          </p>
          <p className="product-card__number">
            Код товару : <span>{this.props.id}</span>
          </p>
          <button className="product-card__btn" onClick={() => this.handleShowModal()}>
            Купити
          </button>
        </li>

        {showModal && (
          <Modal
            addToCart={this.props.addToCart}
            cartItems={this.props.cartItems}
            onClose={this.handleHideModal}
            src={this.props.image}
            text={this.props.name}
            price={this.props.price}
            id={this.props.id}
          />
        )}
      </>
    );
  }
}

export default ProductCard;

ProductCard.propTypes = {
  name: PropTypes.string,
  text: PropTypes.string,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  showModal: PropTypes.bool,
};

ProductCard.defaultProps = {
  text: "no text",
  name: "no name",
  price: 0,
};
