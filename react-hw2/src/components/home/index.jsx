import { Component } from "react";
import Header from "../Header";
import Main from "../main";

!localStorage.getItem("favItems")
  ? localStorage.setItem("favItems", JSON.stringify([]))
  : localStorage.getItem("favItems");

!localStorage.getItem("cartItems")
  ? localStorage.setItem("cartItems", JSON.stringify([]))
  : localStorage.getItem("cartItems");

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardData: null,
      dataLoaded: false,
      favItems: JSON.parse(localStorage.getItem("favItems")),
      cartItems: JSON.parse(localStorage.getItem("cartItems")),
    };

    this.addToFav = this.addToFav.bind(this);
    this.addToCart = this.addToCart.bind(this);
  }

  componentDidMount() {
    fetch("./items.json")
      .then((res) => res.json())
      .then((data) => {
        this.setState({ cardData: data, dataLoaded: true });
      })
      .catch((error) => console.log(error));
  }

  addToFav(id) {
    const arr = this.state.favItems;
    if (!arr.includes(id)) {
      arr.push(id);
    } else {
      const index = arr.findIndex((item) => item === id);
      arr.splice(index, 1);
    }

    localStorage.setItem("favItems", JSON.stringify(arr));
    this.setState({ favItems: arr });
    return arr;
  }

  addToCart(id) {
    const arr = this.state.cartItems;
    if (!arr.includes(id)) {
      arr.push(id);
    }
    localStorage.setItem("cartItems", JSON.stringify(arr));
    this.setState({ cartItems: arr });
    // this.setState({ favorite: arr.includes(id) });
    return arr;
  }

  render() {
    const favItemsArr = this.state.favItems;
    const cartItemsArr = this.state.cartItems;
    return (
      <>
        <Header favItems={favItemsArr} cartItems={cartItemsArr} />
        <Main
          addToCart={this.addToCart}
          addToFav={this.addToFav}
          favItems={favItemsArr}
          cartItems={cartItemsArr}
          cardData={this.state.cardData}
          dataLoaded={this.state.dataLoaded}
        />
      </>
    );
  }
}

export default Home;
