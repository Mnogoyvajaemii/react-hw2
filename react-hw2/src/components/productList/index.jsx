import { Component } from "react";
import ProductCard from "../productCard";
import PropTypes from "prop-types";

import "./productList.scss";

class ProductList extends Component {
  render() {
    const { cardData, dataLoaded } = this.props;
    let itemsList;

    if (!dataLoaded) {
      itemsList = <p>Loading...</p>;
    } else if (cardData && cardData.length > 0) {
      itemsList = cardData.map((item) => {
        return (
          <ProductCard
            addToCart={this.props.addToCart}
            addToFav={this.props.addToFav}
            cartItems={this.props.cartItems}
            favItems={this.props.favItems}
            name={item.name}
            price={item.price}
            color={item.color}
            key={item["item number"]}
            id={item["item number"]}
            image={item.image}
          />
        );
      });
    } else {
      itemsList = <p>No items found.</p>;
    }

    return <ul className="product-list">{itemsList}</ul>;
  }
}

export default ProductList;

ProductList.propTypes = {
  name: PropTypes.string,
  price: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  showModal: PropTypes.bool,
  dataLoaded: PropTypes.bool,
  id: PropTypes.number,
};

ProductCard.defaultProps = {
  text: "no text",
  price: 0,
  name: "no name",
};
