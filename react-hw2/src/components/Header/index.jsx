import { Component } from "react";
import "./header.scss";
import { StarIcon, ShoppingCart } from "../icons";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = { favItems: this.props.favItems, cartItems: this.props.cartItems };
  }

  render() {
    return (
      <>
        <header className="header-page">
          <div className="container header-page--container">
            <div className="header-page__icons">
              <p className="icon-descr">Кошик</p>
              {this.state.cartItems.length > 0 ? (
                <div className="counter">
                  <p>{this.state.cartItems.length}</p>
                </div>
              ) : null}

              <ShoppingCart className={"header-page__icon"} />
            </div>
            <div className="header-page__icons">
              {this.state.favItems.length > 0 ? (
                <div className="counter">
                  <p>{this.state.favItems.length}</p>
                </div>
              ) : null}

              <p className="icon-descr">Обране</p>
              <StarIcon className={"header-page__icon"} />
            </div>
          </div>
        </header>
      </>
    );
  }
}

export default Header;
