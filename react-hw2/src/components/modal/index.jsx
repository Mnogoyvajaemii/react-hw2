import { Component } from "react";
import "./modal.scss";

class Modal extends Component {
  constructor(props) {
    super(props);

    this.handleOverlayClick = this.handleOverlayClick.bind(this);
  }

  handleOverlayClick(event) {
    if (event.target === event.currentTarget) {
      this.props.onClose();
    }
  }

  // addToCart(id) {
  //   const arr = this.props.cartItems;
  //   if (!arr.includes(id)) {
  //     arr.push(id);
  //   }
  //   localStorage.setItem("cartItems", JSON.stringify(arr));
  //   this.setState({ favorite: arr.includes(id) });
  //   return arr;
  // }

  render() {
    return (
      <>
        <div className="modal-overlay" onClick={this.handleOverlayClick}></div>
        <div className="modal">
          <div className="modal-closedBtn" onClick={this.handleOverlayClick}></div>
          <h2 className="modal-title">Підтвердження покупки</h2>
          <div className="modal-item">
            <img src={this.props.src} alt="paint" />
            <p>{this.props.text}</p>
          </div>
          <p className="modal-descr modal-descr--price">{this.props.price} ₴</p>
          <p className="modal-descr">Бажаєте додати товар в кошик ?</p>
          <div className="modal-buttons">
            <button
              onClick={(event) => {
                this.props.addToCart(this.props.id);
                this.handleOverlayClick(event);
              }}
            >
              Додати в кошик
            </button>
            <button onClick={this.handleOverlayClick}>Продовжити покупки</button>
          </div>
        </div>
      </>
    );
  }
}

export default Modal;
