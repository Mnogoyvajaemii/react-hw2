import { Component } from "react";
import ProductList from "../productList";
import "./main.scss";

class Main extends Component {
  render() {
    return (
      <>
        <main className="main-page">
          <div className="container container--products">
            <ProductList
              addToCart={this.props.addToCart}
              addToFav={this.props.addToFav}
              favItems={this.props.favItems}
              cartItems={this.props.cartItems}
              cardData={this.props.cardData}
              dataLoaded={this.props.dataLoaded}
            />
          </div>
        </main>
      </>
    );
  }
}

export default Main;
